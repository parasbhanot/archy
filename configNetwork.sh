#!/bin/bash

echo myarch > /etc/hostname
touch /etc/hosts

echo "127.0.0.1	localhost" > /etc/hosts
echo "::1		localhost" >> /etc/hosts
echo "127.0.1.1	myarch" >> /etc/hosts
