#!/bin/bash

mount /dev/sda2 /mnt
pacstrap /mnt base linux linux-firmware neovim git 

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
