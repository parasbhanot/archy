#!/bin/bash

# timedatectl list-timezones
timedatectl set-timezone Asia/Kolkata
timedatectl set-ntp true
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
